import React from 'react';

const cards = {
    S: ['spades', '♠'],
    H: ['hearts', '♥'],
    C: ['clubs', '♣'],
    D: ['diams', '♦']
};

const Card = (props) => {
    let classes = `card rank-${props.rank.toLowerCase()} ${cards[props.suit][0]}`;
    return (
        <div className={classes}>
            <span className="rank">{props.rank}</span>
            <span className="suit">{cards[props.suit][1]}</span>
        </div>
    )
};

export default Card;