import React, {Component} from 'react';
import Card from './Card/Card';
import './App.css';

class App extends Component {
    state = {
        cards: []
    };
    generateCardDeck = () => {
        const suits = ['S', 'D', 'C', 'H'];
        const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];

        let cardDeck = [];
        for (let s = 0; s < suits.length; s++) {
            for (let r = 0; r < ranks.length; r++) {
                let card = {rank: ranks[r], suit: suits[s]};
                cardDeck.push(card);
            }
        }
        return cardDeck;
    };

    generateRandomCards = () => {
        const deck = this.generateCardDeck();
        let randomCards = [];
        for (let i = 0; i < 5; i++) {
            let rand = Math.floor(Math.random() * deck.length);
            randomCards.push(deck[rand]);
            deck.splice(rand, 1);
        }
        return randomCards;
    };

    componentDidMount = () => {
        this.setState({cards: this.generateRandomCards()});
    };

    shuffleCards = () => {
        let cards = this.generateRandomCards();
        this.setState({cards});
    };

    render() {
        this.generateCardDeck();
        return (
            <div className="playingCards faceImages">
                <button onClick={this.shuffleCards}>Shuffle cards</button>
                <br/><br/>
                {this.state.cards.map((card, index) => <Card rank={card.rank} suit={card.suit} key={index} />)}
            </div>
        );
    }
}

export default App;
